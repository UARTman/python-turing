from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.popup import Popup

from graphics.AlphabetEditPopup import AlphabetEditPopup
from graphics.FileOpPopup import FileOpPopup
from graphics.TapeEditPopup import TapeEditPopup


class ButtonBox(BoxLayout):
    def __init__(self, engine, root, **kwargs):
        super(ButtonBox, self).__init__(orientation="vertical", **kwargs)
        self.popup = None

        self.size_hint = (None, None)
        unit_height = 40
        self.width = 150
        self.height = self.unit_height = unit_height * 4

        self.engine = engine
        self.root = root

        self.l_r_layout = BoxLayout(orientation="horizontal")
        self.left_button = Button(text="Налево", size_hint=(.5, 1))
        self.left_button.bind(on_press=left_button_callback)
        self.right_button = Button(text="Направо", size_hint=(.5, 1))
        self.right_button.bind(on_press=right_button_callback)
        self.l_r_layout.add_widget(self.left_button)
        self.l_r_layout.add_widget(self.right_button)
        self.add_widget(self.l_r_layout)

        self.goto_layout = BoxLayout(orientation="horizontal")
        self.goto_button = Button(text="Переход", size_hint=(.7, 1))
        self.goto_button.bind(on_press=goto_button_callback)
        self.goto_input = TextInput(multiline=False, size_hint=(.3, 1), text='0')
        self.goto_layout.add_widget(self.goto_button)
        self.goto_layout.add_widget(self.goto_input)
        self.add_widget(self.goto_layout)

        self.state_goto_layout = BoxLayout(orientation="horizontal")
        self.state_goto_button = Button(text="Переход-сост", size_hint=(.7, 1))
        self.state_goto_button.bind(on_press=state_goto_button_callback)
        self.state_goto_input = TextInput(multiline=False, size_hint=(.3, 1), text='0')
        self.state_goto_layout.add_widget(self.state_goto_button)
        self.state_goto_layout.add_widget(self.state_goto_input)
        self.add_widget(self.state_goto_layout)

        self.step_button = Button(text="Сделать шаг")
        self.step_button.bind(on_press=step_button_callback)
        self.add_widget(self.step_button)

        self.save_load_layout = BoxLayout(orientation="horizontal")
        self.save_button = Button(text="Сохранить")
        self.load_button = Button(text="Загрузить")
        self.load_button.bind(on_press=load_button_callback)
        self.save_load_layout.add_widget(self.save_button)
        self.save_load_layout.add_widget(self.load_button)
        self.add_widget(self.save_load_layout)

        self.edit_tape_layout = BoxLayout(orientation="horizontal")
        self.edit_tape_button = Button(text="Редактировать ленту")
        self.edit_tape_button.bind(on_press=edit_tape_callback)
        self.edit_tape_layout.add_widget(self.edit_tape_button)
        self.add_widget(self.edit_tape_layout)

        self.edit_alphabet_layout = BoxLayout(orientation="horizontal")
        self.edit_alphabet_button = Button(text="Редактировать алфавит")
        self.edit_alphabet_button.bind(on_press=edit_alphabet_callback)
        self.edit_alphabet_layout.add_widget(self.edit_alphabet_button)
        self.add_widget(self.edit_alphabet_layout)

        self.file_op_layout = BoxLayout(orientation="vertical")
        self.file_op_button = Button(text="Сохранить/Загрузить")
        self.file_op_button.bind(on_press=file_op_callback)
        self.file_op_layout.add_widget(self.file_op_button)
        self.add_widget(self.file_op_layout)


def left_button_callback(instance):
    instance.parent.parent.engine.tape.move(-1)
    instance.parent.parent.root.update_text()


def right_button_callback(instance):
    instance.parent.parent.engine.tape.move(1)
    instance.parent.parent.root.update_text()


def goto_button_callback(instance):
    instance.parent.parent.engine.tape.move(int(instance.parent.parent.goto_input.text) -
                                            instance.parent.parent.engine.tape.pos)
    instance.parent.parent.root.update_text()


def state_goto_button_callback(instance):
    instance.parent.parent.engine.r_w_m.pos = int(instance.parent.parent.state_goto_input.text)
    instance.parent.parent.root.update_text()


def step_button_callback(instance):
    instance.parent.engine.step()
    instance.parent.root.update_text()


def load_button_callback(instance):
    instance.parent.parent.engine.load_alphabet_from_file(input("Alphabet File: "))
    instance.parent.parent.engine.load_logic_from_file(input("Logic File: "))
    instance.parent.parent.engine.load_tape_from_file(input("Tape File: "))
    instance.parent.parent.root.update_text()


def edit_tape_callback(instance):
    c = TapeEditPopup(bbox=instance.parent.parent)
    popup = Popup(content=c, size_hint=(None, None), width=500, height=100, title="Редактирование ленты")
    popup.open()


def edit_alphabet_callback(instance):
    c = AlphabetEditPopup(bbox=instance.parent.parent)
    popup = Popup(content=c, size_hint=(None, None), width=500, height=100, title="Редактирование ленты")
    popup.open()


def file_op_callback(instance):
    c = FileOpPopup(bbox=instance.parent.parent)
    popup = Popup(content=c, size_hint=(None, None), width=500, height=500, title="Файловые операции")
    popup.open()
