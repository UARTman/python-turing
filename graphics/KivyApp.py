from kivy.app import App

from graphics.RootWidget import RootWidget


class KivyApp(App):
    def __init__(self, engine, **kwargs):
        super(KivyApp, self).__init__(**kwargs)
        self.engine = engine

    def build(self):
        fgrid = RootWidget(self.engine)
        return fgrid
