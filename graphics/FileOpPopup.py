from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.popup import Popup
from kivy.uix.filechooser import FileChooserListView

from graphics.FilePopup import FilePopupLayout


class FileOpPopup(BoxLayout):
    def __init__(self, bbox, **kwargs):
        super(FileOpPopup, self).__init__(**kwargs)
        self.bbox = bbox
        self.SaveButton = Button(text="Сохранить")
        self.SaveButton.bind(on_press=save_callback)
        self.add_widget(self.SaveButton)
        self.LoadButton = Button(text="Загрузить")
        self.LoadButton.bind(on_press=load_callback)
        self.add_widget(self.LoadButton)


class SelectPopup(BoxLayout):
    def __init__(self, mode, **kwargs):
        super(SelectPopup, self).__init__(**kwargs)
        self.mode = mode
        self.AlphaButton = Button(text="Alphabet")
        self.AlphaButton.bind(on_press=select_callback)
        self.add_widget(self.AlphaButton)
        self.LogicButton = Button(text="Logic")
        self.LogicButton.bind(on_press=select_callback)
        self.add_widget(self.LogicButton)
        self.TapeButton = Button(text="Tape")
        self.TapeButton.bind(on_press=select_callback)
        self.add_widget(self.TapeButton)


def save_callback(instance):
    c = SelectPopup(mode="s")
    popup = Popup(content=c, size_hint=(None, None), width=500, height=500, title="What to save?")
    c.popup = popup
    popup.open()

def load_callback(instance):
    c = SelectPopup(mode="l")
    popup = Popup(content=c, size_hint=(None, None), width=500, height=500, title="What to save?")
    c.popup = popup
    popup.open()


def select_callback(instance):
    if instance.parent.mode == 's':
        a = FilePopupLayout(print)
    if instance.parent.mode == 'l':
        a = FilePopupLayout(print)
    popup = Popup(content=a, size_hint=(None, None), width=500, height=500, title="What to save?")
    a.popup = popup
    popup.open()


