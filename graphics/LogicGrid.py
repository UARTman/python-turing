from kivy.uix.textinput import TextInput
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label


class LogicGrid(GridLayout):
    def __init__(self, engine, **kwargs):
        super(LogicGrid, self).__init__(**kwargs)
        self.engine = engine
        self.logic = None
        self.alphabet = None
        self.generate_layout()

    def generate_layout(self):
        self.logic = (self.engine.def_state, self.engine.num_states, self.engine.logic)
        self.alphabet = self.engine.alphabet
        self.size_hint = (None, None)
        self.cols = self.logic[1]
        self.rows = len(self.alphabet[1]) + 1
        self.width = self.cols * 80
        self.height = self.rows * 30
        self.clear_widgets()
        self.add_widget(Label())
        for i in range(self.logic[1] - 1):
            self.add_widget(Label(text=str(i)))
        for i in self.alphabet[1]:
            self.add_widget(Label(text=i))
            for j in range(self.logic[1] - 1):
                a = TextInput()
                if (j, i) in self.logic[2]:
                    a.text = str(self.logic[2][(j, i)])
                else:
                    a.text = '-'
                self.add_widget(a)
