from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput


class TapeEditPopup(BoxLayout):
    def __init__(self, bbox, **kwargs):
        super(TapeEditPopup, self).__init__(**kwargs)
        self.bbox = bbox
        self.tape_input = TextInput(text=self.bbox.engine.read_tape(), multiline=False)
        self.tape_input.bind(text=tape_input_callback)
        self.add_widget(self.tape_input)


def tape_input_callback(instance, value):
    instance.parent.bbox.engine.write_tape(value)
    instance.parent.bbox.root.update_text()
