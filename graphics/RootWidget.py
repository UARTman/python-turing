from kivy.uix.boxlayout import BoxLayout
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.textinput import TextInput

from graphics.FieldGrid import FieldGrid
from graphics.ButtonBox import ButtonBox
from graphics.LogicGrid import LogicGrid


class RootWidget(BoxLayout):
    def __init__(self, engine, **kwargs):
        super(RootWidget, self).__init__(orientation='vertical', **kwargs)
        self.engine = engine

        self.field_grid = FieldGrid(engine)
        self.add_widget(self.field_grid)

        self.bottom_layout = BoxLayout(orientation="horizontal")
        self.table_layout = AnchorLayout(anchor_x="left", anchor_y="top")
        self.logic_grid = LogicGrid(engine)
        self.table_layout.add_widget(self.logic_grid)
        self.bottom_layout.add_widget(self.table_layout)

        self.button_layout = AnchorLayout(anchor_x="right", anchor_y="top")
        self.button_box = ButtonBox(engine, self)
        self.button_layout.add_widget(self.button_box)
        self.bottom_layout.add_widget(self.button_layout)

        self.add_widget(self.bottom_layout)
        self.update_text()

    def update_text(self):
        self.field_grid.update_text()
        self.logic_grid.generate_layout()
