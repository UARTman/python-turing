from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput


class AlphabetEditPopup(BoxLayout):
    def __init__(self, bbox, **kwargs):
        super(AlphabetEditPopup, self).__init__(**kwargs)
        self.bbox = bbox
        self.alphabet_input = TextInput(text=self.bbox.engine.get_alphabet(), multiline=False)
        self.alphabet_input.bind(text=alphabet_input_callback)
        self.add_widget(self.alphabet_input)


def alphabet_input_callback(instance, value):
    instance.parent.bbox.engine.make_alphabet_from_string(value)
    instance.parent.bbox.root.update_text()
