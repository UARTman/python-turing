from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.filechooser import FileChooserListView
from kivy.uix.textinput import TextInput


class FilePopupLayout(BoxLayout):
    def __init__(self, fun, **kwargs):
        super(FilePopupLayout, self).__init__(orientation="vertical", **kwargs)
        self.fun = fun
        self.FileChooser = FileChooserListView(path=".")
        self.add_widget(self.FileChooser)
        self.SelectInput = TextInput(size_hint=(1, 0.1))
        self.SelectInput.bind(text=input_callback)
        self.add_widget(self.SelectInput)
        self.SelectButton = Button(text="Ok", size_hint=(1, 0.1))
        self.SelectButton.bind(on_press=select_callback)
        self.add_widget(self.SelectButton)
        self.popup = None


def select_callback(instance):
    instance.parent.fun(instance.parent.FileChooser.selection[0])
    instance.parent.popup.dismiss()


def input_callback(instance, value):
    if len(instance.parent.FileChooser.selection):
        instance.parent.FileChooser.selection[0] = instance.parent.FileChooser.path + '/' + value
    else:
        instance.parent.FileChooser.selection = [instance.parent.FileChooser.path + '/' + value]
