from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput


class FieldGrid(GridLayout):
    def __init__(self, engine, **kwargs):
        super(FieldGrid, self).__init__(**kwargs)
        self.height = 60
        self.size_hint_y = None
        self.cols = 3
        self.rows = 2

        self.engine = engine

        self.add_widget(Label(text="Лента"))
        self.add_widget(Label(text="Указатель", size_hint_x=None, width=100))
        self.add_widget(Label(text="Состояние", size_hint_x=None, width=100))

        self.tapeInput = TextInput(multiline=False)
        self.pointerInput = TextInput(multiline=False, size_hint_x=None, width=100)
        self.stateInput = TextInput(multiline=False, size_hint_x=None, width=100)
        self.add_widget(self.tapeInput)
        self.add_widget(self.pointerInput)
        self.add_widget(self.stateInput)

    def update_text(self):
        self.tapeInput.text = self.engine.read_tape()
        self.pointerInput.text = str(self.engine.tape.pos)
        self.stateInput.text = str(self.engine.r_w_m.pos)
