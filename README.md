# Turing machine interpreter running on Python
## Dependencies
 - Python 3
## Running
 - Run `main.py`
## Configuring and programming
Your "program" is composed from three files:
 - Alphabet file (`alphabet.txt`)
   - This file should contain all possible symbols on your tape.
   - First symbol should be an "empty" symbol.
   - There are symbols than can not be used in tape due to syntax: `=` and ` `
 - Tape file (`tape.txt`)
   - Should contain one string with tape contents on the start.
 - Logic file (`logic.txt`)
   - Sample contents:
```
{start_state}/{end_state}
#instructions
{S1} {B1} do {S2} {B2} {M}
{S1} {B1} do {S2} {B2} {M}
{S1} {B1} do {S2} {B2} {M}
         ...             
{S1} {B1} do {S2} {B2} {M}
```
Where 
```
start_state - starting state of read-writing machine
end_state - end condition and max available state 
S1 - current state
B1 - current tape reading
S2 - result state
B1 - write this on tape
M - movement after this
```
