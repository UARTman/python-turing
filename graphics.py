from graphics.KivyApp import KivyApp

from engine import Engine

if __name__ == "__main__":
    engine = Engine()
    KivyApp(engine).run()