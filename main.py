# -*- coding: utf-8 -*-
from engine import Engine

if __name__ == '__main__':
    engine = Engine()
    engine.load_alphabet_from_file(input("Alphabet File: "))
    engine.load_logic_from_file(input("Logic File: "))
    engine.load_tape_from_file(input("Tape File: "))

    engine.run()

    print(engine.read_tape())
