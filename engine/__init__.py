from engine.RWM import RWM
from engine.Tape import Tape

DEFAULT_ALPHABET = ['_', {'_', '1'}]
DEFAULT_LOGIC = (0, 2, {(0, '_'): (1, '1', 0)})
DEFAULT_TAPE = "_"


def a_split(s):
    c = s.split("do")
    return list(map(b_split, c))


def b_split(s):
    s = s.split()
    if len(s) == 2:
        return int(s[0]), s[1]
    if len(s) == 3:
        return int(s[0]), s[1], int(s[2])


def get_logic(fn, dct):
    ret = {}
    with open(fn, 'r') as f:
        b = f.read()
    b = b.split("\n#instructions\n")
    c = b[0].split("/")
    d = b[1].split("\n")
    d = list(map(a_split, d))
    for i in d:
        if i[0] is None:
            continue
        assert (i[0][1] in dct[1]) and (i[1][1] in dct[1]), "Trying to write a character not in alphabet"
        ret[i[0]] = i[1]
    return (int(c[0]), int(c[1])), ret


def get_alphabet(fn):
    ret = set()
    file = None
    with open(fn, 'r') as fl:
        for i in fl.read().replace('\n', ''):
            ret.add(i)
            if file is None:
                file = i
    return file, ret

def gen_alphabet(inp):
    ret = set()
    file = None
    for i in inp:
        ret.add(i)
        if file is None:
            file = i
    return file, ret

class Engine:
    def __init__(self, alphabet=None, def_state=DEFAULT_LOGIC[0],
                 num_states=DEFAULT_LOGIC[1], logic=None):
        if logic is None:
            logic = DEFAULT_LOGIC[2]
        if alphabet is None:
            alphabet = DEFAULT_ALPHABET
        self.alphabet = alphabet
        self.def_state = def_state
        self.num_states = num_states
        self.logic = logic
        self.r_w_m = RWM(alphabet, logic, def_state, num_states)
        tape = self.alphabet[0]
        self.tape = Tape(self.alphabet, tape, 0)

    def write_tape(self, inp):
        self.tape.s = list(inp)

    def load_tape_from_file(self, fn):
        with open(fn, 'r') as f:
            s = f.read().replace('\n', '')
        self.write_tape(s)

    def read_tape(self):
        return_value = ""
        for i in self.tape.s:
            return_value += i
        return return_value

    def save_tape_to_file(self, fn):
        s = self.read_tape()
        with open(fn, "w") as f:
            f.write(s)

    def set_logic(self, def_state, num_states, logic):
        self.def_state = def_state
        self.num_states = num_states
        self.r_w_m.s_pos = def_state
        self.r_w_m.end = num_states
        self.logic = logic
        self.r_w_m.logic = logic

    def load_logic_from_file(self, fn):
        ((def_state, num_states), logic) = get_logic(fn, self.alphabet)
        self.set_logic(def_state, num_states, logic)

    def save_logic_to_file(self, fn):
        with open(fn, 'w') as f:
            print("{0}/{1}".format(str(self.def_state), str(self.num_states)), file=f)
            for i in range(self.num_states):
                for j in self.alphabet:
                    if (i, j) in self.logic:
                        print(i, j, 'do', self.logic[0], self.logic[1], self.logic[2], file=f)

    def set_alphabet(self, alphabet):
        self.alphabet = alphabet
        self.r_w_m.alphabet = alphabet
        self.tape.alphabet = alphabet

    def make_alphabet_from_string(self, inp):
        self.set_alphabet(gen_alphabet(inp))

    def get_alphabet(self):
        return_value = self.alphabet[0]
        for i in self.alphabet[1]:
            if i not in return_value:
                return_value += i
        return return_value

    def load_alphabet_from_file(self, fn):
        self.set_alphabet(get_alphabet(fn))

    def step(self):
        return self.r_w_m.step(self.tape)

    def run(self):
        while not self.step():
            pass
