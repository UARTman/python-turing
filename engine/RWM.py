class RWM:
    def __init__(self, alphabet, logic, pos, end):
        self.alphabet = alphabet
        self.logic = logic
        self.pos = pos
        self.s_pos = pos
        self.end = end

    def step(self, tape):
        if self.pos == self.end:
            return True
        logic = self.logic[(self.pos, tape.get())]
        self.pos = logic[0]
        tape.put(logic[1])
        tape.move(logic[2])
        return False

    def reset(self):
        self.pos = self.s_pos