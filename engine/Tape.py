class Tape:
    def __init__(self, alphabet, s, pos):
        self.alphabet = alphabet
        self.s = list(s)
        self.pos = pos
        self.nul = self.alphabet[0]

    def put(self, target):
        self.s[self.pos] = target

    def get(self):
        return self.s[self.pos]

    def move(self, m):
        if m:
            if abs(m) > 1:
                for i in range(abs(m)):
                    self.move(m // abs(m))
                return
            length = len(self.s)
            self.pos += m
            if self.pos == -1:
                self.s.insert(0, self.nul)
                self.pos = 0
            elif self.pos == length:
                self.s.append(self.nul)
            elif (self.pos == 1) and (self.s[0] == self.nul):
                self.s.pop(0)
                self.pos = 0
            elif (self.pos == (length - 2)) and (self.s[length - 1] == self.nul):
                self.s.pop()
